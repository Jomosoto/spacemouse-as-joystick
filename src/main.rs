use spacenav::Event;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut spcnav = spacenav::SpaceNav::new().unwrap();
    let joystick = software_joystick::Joystick::new(String::from("3D Mouse"))?;

    loop {
        if let Some(event) = spcnav.read() {
            match event {
                Event::ButtonPress(index) => {
                    joystick.button_press(index_to_button(index), true)?;
                }

                Event::ButtonRelease(index) => {
                    joystick.button_press(index_to_button(index), false)?;
                }

                Event::Motion {
                    x,
                    y,
                    z,
                    rx,
                    ry,
                    rz,
                    period: _,
                } => {
                    use software_joystick::Axis::*;
                    joystick.move_axis(X, value_sm_to_joystick(x))?;
                    joystick.move_axis(Y, value_sm_to_joystick(y))?;
                    joystick.move_axis(Z, value_sm_to_joystick(z))?;
                    joystick.move_axis(RX, value_sm_to_joystick(rx))?;
                    joystick.move_axis(RY, value_sm_to_joystick(ry))?;
                    joystick.move_axis(RZ, value_sm_to_joystick(rz))?;
                }
            };
        }
    }
}

fn value_sm_to_joystick(value: i32) -> i32 {
    (f64::from(value) / 350.0 * 512.0).round() as i32
}

fn index_to_button(index: i32) -> software_joystick::Button {
    use software_joystick::Button::*;
    match index {
        0 => RightSouth,
        1 => RightEast,
        2 => RightWest,
        3 => RightNorth,
        4 => LeftSouth,
        5 => LeftEast,
        6 => LeftWest,
        7 => LeftNorth,
        8 => L1,
        9 => R1,
        10 => L2,
        11 => R2,
        12 => RightSpecial,
        13 => LeftSpecial,
        _ => RightSouth,
    }
}
